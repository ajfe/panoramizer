#!/usr/bin/env python

import sys
import libxmp
import Image
import argparse

# evaluates argument 'a', if it's a float and satisfies the 'interval_validator' function, returns true
def validate_interval(a,interval_validator):
    try:
        value = float(a)
        return interval_validator(value)
    except ValueError:
            return False
    
# check if argument is a float and in interval ]0.0-100.0]
def float_percentage(fl):
    
    msg_base = "%r is not a valid percentage > 0 [0.0-100.0]"

    if validate_interval(fl,lambda x: x > 0.0 and x <= 100.0):
        return float(fl)
    else:
        raise argparse.ArgumentTypeError(msg_base % fl)
        
# check if argument is a float and in interval [0.0-360.0]
def float_arc360(fl):
    
    msg_base = "%r is not a valid arc [0.0-360.0]"

    if validate_interval(fl,lambda x: x >= 0.0 and x <= 360.0):
        return float(fl)
    else:
        raise argparse.ArgumentTypeError(msg_base % fl)

# parse the command-line arguments, returns the parsed result or exits if an error exists
def parseCmdLineArgs():
    parser = argparse.ArgumentParser(description='Add XMP metadata to a .jpg image to make it compatible with Google Photosphere panoramas.')
    
    parser.add_argument('-W', '--widthscale', type=float_percentage, default=100.0, required=False,
                   help='Set the width of the picture in the panorama, as a fraction of the full width - [0.0-100.0]*360 degrees.\
                   If omitted, the picture will take up the full width of the panorama (360 deg.).\
                   Value must be > 0.')

    parser.add_argument('-H', '--heightscale', type=float_percentage, default=100.0, required=False,
                   help='Set the height of the picture in the panorama, as a fraction of the full height - [0.0-100.0]*180 degrees.\
                   If omitted, the picture will take up the full height of the panorama (180 deg.).\
                   Value must be > 0.')

    parser.add_argument('-i', '--initialheading', type=float_arc360, default=0.0, required=False,
                   help='Set the initial heading (direction) from the center of the image that the view points at when loading the panorama (degrees).')

    parser.add_argument('filename', action='store', 
                   help='The filename to panoramize. The original file is modified by this script.')

    return parser.parse_args()

## load file
## if it doesn't have the 'GPano' namespace, add it
## get image resolution
## set GPano namespace properties accordingly

# returns a dict with the bare minimum XMP to make the image work as a panorama 
# heading: where the view points at, initially
# w_scale / h_scale: determine how much of the sphere is covered by the picture
# in fact, it will scale the sphere in proportion to the scale parameters, from [img_w,img_h] (100% scale) to [inf,inf] (0% scale)
# croppedwidth = img_w
# croppedheight = img_h
# fullpanoramawidth = img_w / w_scale
# fullpanoramaheight = img_h / h_scale
# leftpixels = (full_w - img_w) / 2
# toppixels = (full_h - img_h) / 2
def buildBareXmp (img_w, img_h, w_scale, h_scale, heading):

    w_scale = w_scale / 100
    h_scale = h_scale / 100
    full_w = int(round(img_w / w_scale))
    full_h = int(round(img_h / h_scale))
    leftpx = int(round((full_w - img_w) / 2))
    toppx = int(round((full_h - img_h) / 2))
    
    print "img_w: {0}, img_h: {1}, w_scale: {2}, h_scale: {3}, heading: {4}.".format(img_w,img_h,w_scale,h_scale,heading)
    print "full_w: {0}, full_h: {1}, left: {2}, top: {3}.".format(full_w, full_h,leftpx,toppx)

    xmpdict = {
        'GPano:UsePanoramaViewer': True,
        'GPano:ProjectionType': "equirectangular",
        'GPano:PoseHeadingDegrees': heading,
        'GPano:CroppedAreaImageWidthPixels': img_w,
        'GPano:CroppedAreaImageHeightPixels': img_h,
        'GPano:FullPanoWidthPixels': full_w,
        'GPano:FullPanoHeightPixels': full_h,
        'GPano:CroppedAreaLeftPixels': leftpx,
        'GPano:CroppedAreaTopPixels': toppx
    }
    
    return xmpdict

def main():
    
    default_ns_prefix = "GPano"
    default_ns_uri = "http://ns.google.com/photos/1.0/panorama/"

    args = parseCmdLineArgs()
    xmpfilename = args.filename
    scale_w = args.widthscale
    scale_h = args.heightscale
    initialheading = args.initialheading
    
    try:
        xmpfile = libxmp.XMPFiles( file_path=xmpfilename, open_forupdate=True )
        xmp = xmpfile.get_xmp()
        
        ns = xmp.get_namespace_for_prefix(default_ns_prefix)
                
        if not ns:
            xmp.register_namespace(default_ns_uri,default_ns_prefix)
            ns = xmp.get_namespace_for_prefix(default_ns_prefix)
            print "GPano namespace: not present. Added..."
        else:
            print "GPano namespace: present"

        im = Image.open(xmpfilename)
        xmp_newprops = buildBareXmp(im.size[0], im.size[1],scale_w,scale_h,initialheading)
        
        for prop in xmp_newprops.keys():
            val = xmp_newprops[prop]
            print "Setting property {0} = {1}".format(prop,val)
            xmp.set_property(ns, prop, str(val))
        
        if xmpfile.can_put_xmp(xmp):
            print "Writing to {0}".format(xmpfilename)
            xmpfile.put_xmp(xmp)
            
        xmpfile.close_file()
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        exit(1)
    except libxmp.XMPError as e:
        print "XMP error({0})".format(e)
        exit(1)
                

if __name__ == "__main__":
    main()
