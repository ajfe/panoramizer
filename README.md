## Panoramizer is a script that will add the necessary XMP metadata to a .JPG file to make it compatible with the google panoramas (aka Photosphere).

[Google's GPano namespace XMP reference][1]

Usage
-----

	usage: panoramize.py [-h] [-W WIDTHSCALE] [-H HEIGHTSCALE] [-i INITIALHEADING] filename

	Add XMP metadata to a .jpg image to make it compatible with Google Photosphere panoramas.

	positional arguments:
	  filename              The filename to panoramize. The original file is modified by this script.

	optional arguments:
	  -h, --help            show this help message and exit
	  -W WIDTHSCALE, --widthscale WIDTHSCALE
                        Set the width of the picture in the panorama, as a fraction of the full width - [0.0-100.0]*360 degrees.
	                If omitted, the picture will take up the full width of the panorama (360 deg.). Value must be > 0.

	  -H HEIGHTSCALE, --heightscale HEIGHTSCALE
                        Set the height of the picture in the panorama, as a fraction of the full height - [0.0-100.0]*180 degrees.
                        If omitted, the picture will take up the full height of the panorama (180 deg.). Value must be > 0.

	  -i INITIALHEADING, --initialheading INITIALHEADING
                        Set the initial heading (direction) from the center of the image that the view points at when loading the panorama (degrees).

Examples
--------

	(project image in a 180-degree horizontal arc and 90-degree vertical arc)
	panoramize.py -W 50 -H 50 sample.jpg

	(project the image onto a full sphere)
	panoramize.py sample.jpg

Notes
-----

- The sphere's resolution is actually expanded to accomodate the desired proportions, so that the original image's resolution is kept (i.e. for a 2048x2048px .jpg with -W 50, the resulting sphere will be 4096x2048px, where the extra width of 2048px is empty space).
- Using very low -W and -H values will result in very large width and height values for the sphere, which may cause performance issues.
- The actual resolution of the image is left unchanged, all manipulation is done in metadata.

[1]: https://developers.google.com/panorama/metadata/
