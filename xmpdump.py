#!/usr/bin/env python

import sys
import libxmp

def main():
    
    try:
        xmpfilename = sys.argv[1]
    except IndexError:
        print "Must provide a filename to read from."
        exit(1)

    try:
#    xmpfile = libxmp.XMPFiles( file_path=xmpfilename, open_forupdate=False )
#    xmp = xmpfile.get_xmp()
    
        xmpdict = libxmp.utils.file_to_dict(xmpfilename)
        namespaces = xmpdict.keys()
    
        print "Namespaces\n{0}\n".format("\n".join(namespaces))
   
        for ns in namespaces:
            #propnames = [t[0] for t in xmpdict[ns]]
            props = xmpdict[ns]
            properties = dict(sorted([[t[0],t[1]] for t in props],key=lambda tup: tup[1]))
            
            print "Properties for namespace {0}:\n".format(ns)
            for k, v in properties.items():
                print "{0} = {1}".format(k,v)
        
            print "###############################\n"
        
            #ns = xmp.get_namespace_for_prefix("GPano")
            #print (xmp.get_property(ns,"GPano:FullPanoWidthPixels"))
    
            #xmpfile.close_file()
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        exit(1)
    except libxmp.XMPError as e:
        print "XMP error({0}): {1}".format(e.errno, e.strerror)
        exit(1)
                

if __name__ == "__main__":
    main()
